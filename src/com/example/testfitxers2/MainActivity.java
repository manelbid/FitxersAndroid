package com.example.testfitxers2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {

	private String data;
	private File file;
	EditText et1;
	EditText et2;
	EditText et3;
	EditText et4;
	EditText et5;
	File files[];

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		et1 = (EditText) (findViewById(R.id.editText1));
		et2 = (EditText) (findViewById(R.id.editText2));
		et3 = (EditText) (findViewById(R.id.editText3));
		et4 = (EditText) (findViewById(R.id.editText4));
		et5 = (EditText) (findViewById(R.id.editText5));
		files = Environment.getExternalStorageDirectory().listFiles();
	}

	public void save(View view) {
		if (Environment.MEDIA_REMOVED.equals(Environment
				.getExternalStorageState())) {
			Toast.makeText(this, "No hi ha SD card", Toast.LENGTH_SHORT).show();
		} else {
			// fitxer creant una carpeta privada d'app a SD
			// (Android/data/app/files)
			// getExternalFilesDir("Documentos"), "prueba.txt")
			// fitxer a l'arrel de la SD
			file = new File(Environment.getExternalStorageDirectory(), et2
					.getText().toString());
			data = et3.getText().toString();

			OutputStreamWriter fOut;
			try {
				fOut = new OutputStreamWriter(new FileOutputStream(file));
				fOut.append(data);
				fOut.flush();
				fOut.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void read(View view) {
		files = Environment.getExternalStorageDirectory().listFiles();
		for (File f : files) {
			if (et1.getText().toString().equals(f.getName())) {
				try {
					FileInputStream fin = openFileInput(f.getName());
					int c;
					String temp = "";
					while ((c = fin.read()) != -1) {
						temp = temp + Character.toString((char) c);
					}
					et4.setText(temp);
				} catch (Exception e) {
				}
			}
		}
	}

	public void delete(View view) {
		files = Environment.getExternalStorageDirectory().listFiles();
		for (File f : files) {
			if (et5.getText().toString().equals(f.getName())) {
				f.delete();
			} else {
				Toast.makeText(this, "El fitxer no existeix",
						Toast.LENGTH_SHORT).show();
			}
		}

	}

}
